# Capitulo 4 - Transformações (Prática)

Agora vamos colocar o que você aprendeu no capitulo passado.  
O OpenGL não tem nada disso pronto, nós mesmos precisamos de implementar as matrizes, vetores e transformações.  
Felizmente existe a biblioteca [JOML (Java OpenGL Math Library)](https://github.com/JOML-CI/JOML) para nos ajudar,  
Ele tem classes para matrizes, vetores, quartenions, transformações, etc..., e é super facil de usar
```java
Vector3f v = new Vector3f(0.0f, 1.0f, 0.0f);
Vector3f a = new Vector3f(1.0f, 0.0f, 0.0f);
// v = v + a
// somar 2 vetores
v.add(a);
// a = a x v
// Calcular produto cruzado do vetor a com o vetor v
a.cross(v);
// a = a/|a|
//Normalizar o vetor
a.normalize();


//Exemplo de matriz de perspectiva
Matrix4f projectionMatrix = new Matrix4f();
// Pegar tamanho da tela
IntBuffer w = BufferUtils.createIntBuffer(1);
IntBuffer h = BufferUtils.createIntBuffer(1);
glfwGetWindowSize(idJanela, w, h);
float width = w.get(0);
float height = h.get(0);
// Configurar matrix de perspectiva
projectionMatrix.perspective(/*Fov em radians*/ Math.toRadians(70) , /*aspectRatio*/ width/height, /*near plane*/ 0.1f, /*Far plane*/ 1000f);
```

## **Mão na massa!**
Agora vamos de fato começar a usar as matrizes para mover, rotacionar e aumentar/diminuir os nossos objetos

### **Aplicar projeção**
Antes de fazer isso, vamos aplicar a matrix de perspectiva.  
E para isso precisamos passar a matriz para o nosso vertex shader, mas como?  

#### **Uniforms**
Os uniforms são como variaveis globais, que são o mesmo para todos os vertices do VAO, o que é otimo para matrizes  
Então vamos atualizar o nosso vertex shader para comportar o novo uniform:
```glsl
#version 330 core
layout (location = 0) in vec2 position;
uniform mat4 matrizProjecao;
void main(){
    gl_Position = matrizProjecao * vec4(position, 0.1f, 1);
}
```
Agora nós estamos pegando a matrix do uniform e multiplicando pela posição do vertice

#### **Criando matrix de projeção**
Agora vamos usar o JOML para criar a nossa matriz de projeção  
> Nota: O codigo daqui é suposto estar dentro do game loop, antes de desenhar o frame, porque a janela é redimensionavel

Criamos a matriz  
```java
Matrix4f matrixProjecao = new Matrix4f();
```
Pegamos o tamanho da tela
```java
// Criar buffer pro width
IntBuffer w = BufferUtils.createIntBuffer(1);
// Criar buffer pro height
IntBuffer h = BufferUtils.createIntBuffer(1);
// Pegar pelo GLFW
glfwGetWindowSize(idJanela, w, h);
//E guardar os valores dos buffers em 2 variaveis 
float width = w.get(0);
float height = h.get(0);
```
E de fato aplicamos a projeção
```java
matrixProjecao.perspective(/*Fov em graus*/ Math.toRadians(70) , /*aspectRatio*/ width/height, /*near plane*/ 0.1f, /*Far plane*/ 1000f);
```
Pronto! Agora precisamos de enviar para o shader
```java
glUniformMatrix4fv(glGetUniformLocation(program /*Nome do programa*/, "matrizProjecao" /*Nome do uniform*/), /*Transpor a matriz?*/ false, matrixProjecao.get(new float[16])/*Matriz em array/buffer*/);

```
### Mover objetos

O JOML contem os metodos `Matrix4f#translate`, `Matrix4f#rotate` e `Matrix4f#scale`  
Que podemos usar para transformar os objetos:
```java
Matrix4f transform = new Matrix4f();
transform.translate(2,1,-5);//Move para a posição x: 2, y: 1, z: -5
transform.rotate(Math.PI/4, 1, 1, 0)//Roda 45º no eixo x e y
transform.scale(2.0);//2x maior
```
Agora é preciso fazer upload da matrix no shader:
```java
    glUniformMatrix4fv(glGetUniformLocation(program, "matrizTransform"), false, transform.get(new float[16]));
```
Depois adicionamos no vertex shader:
```java
#version 330 core
layout (location = 0) in vec2 position;
uniform mat4 matrizProjecao;
uniform mat4 matrizTransform;
void main(){
    // É preciso estar exatamente nessa ordem
    // porque o resultado muda dependendo da ordem, ao contrario e multiplicação de numeros
    gl_Position = matrizTransform * matrizProjecao * vec4(position, 0.1f, 1);
}
```
### Going 3D
Primeiro, é preciso ir no codigo que vincula o vbo ao VAO, e mudar o tamanho do vetor para 3:
```java
int vaoHandle = glGenVertexArrays();
glBindVertexArray(vaoHandle);
glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
glVertexAttribPointer( 0, /*Change me to this:*/ 3, GL_FLOAT, false, 0, 0);
glBindBuffer(GL_ARRAY_BUFFER, 0);
glBindVertexArray(0);
``` 
Agora vamos colocar uma forma mais interessante
```java
float[] verticesPositions = {
                -0.5f, -0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f,  0.5f, -0.5f,
                0.5f,  0.5f, -0.5f,
                -0.5f,  0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,

                -0.5f, -0.5f,  0.5f,
                0.5f, -0.5f,  0.5f,
                0.5f,  0.5f,  0.5f,
                0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,
                -0.5f, -0.5f,  0.5f,

                -0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,
                -0.5f, -0.5f, -0.5f,
                -0.5f, -0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,

                0.5f,  0.5f,  0.5f,
                0.5f,  0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f, -0.5f,  0.5f,
                0.5f,  0.5f,  0.5f,

                -0.5f, -0.5f, -0.5f,
                0.5f, -0.5f, -0.5f,
                0.5f, -0.5f,  0.5f,
                0.5f, -0.5f,  0.5f,
                -0.5f, -0.5f,  0.5f,
                -0.5f, -0.5f, -0.5f,

                -0.5f,  0.5f, -0.5f,
                0.5f,  0.5f, -0.5f,
                0.5f,  0.5f,  0.5f,
                0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f,  0.5f,
                -0.5f,  0.5f, -0.5f
        };
```
Agora vamos colocar uma cor dependendo da posição do vertice usando os shaders e mudar o position para vec3:
```java
    String vertexSrc = """
            #version 330 core
            // mudar para vec3       \/
            layout (location = 0) in vec3 position;
            uniform mat4 matrizProjecao;
            uniform mat4 matrizTransform;
            out vec4 color;
            void main(){
                color = vec4(position, 1);
                gl_Position =  matrizProjecao*matrizTransform *vec4(position, 1);
            }
            """;
    String fragmentSrc = """
            #version 330 core
            out vec4 FragColor;
            in vec4 color;
            void main(){
                FragColor = color;
            }
            """;
```
E...  
![img](Chapter4-output.png)

O que ta acontecendo?
O opengl não tem a garantia que vai desenhar os triangulos do mais longe para o mais proximo,  
Então a gente precisa ativar o Depth Testing
```java
glEnable(GL_DEPTH_TEST)
```
Basicamente, antes de desenhar um pixel de um triangulo, ele vai checkar se tem outro fragmento no mesmo lugar que está mais proximo na camera, e ele guarda a coordenada Z dos fragmentos dentro de um buffer então, assim como o color buffer tambem é preciso limpar antes de desenhar:
```java
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
```
E pronto!  
![img](Chapter4-output-final.png)

# Resumo:
Você pode mover objetos no opengl usando matrizes, e passar a matriz para o shader usando uniforms.

Você pode ativar o Depth Testing para prevenir que triangulos que estão mais longe, sobrescrevam pixeis de triangulos mais perto
# Proximo: [Capitulo 5 - First Person Camera](Chapter5-FPS.md)