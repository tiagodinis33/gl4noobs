# Capitulo 0 - Aquecendo

Antes de partir para o codigo, eu preciso de marretar alguns conceitos na sua cabeça
## Não é uma biblioteca
Ele é apenas uma especificação, é só um papel que diz como todas as funçoes e estados devem agir, então a implementação fica com os desenvolvedores dos drivers e fabricantes

Então dependendo do driver da placa de video, algumas coisas podem não agir com o padrão

## OpenGL é uma maquina de estados (Contexto opengl)
O OpenGL funciona como uma maquina de estados. 
Ele tem o conceito de um contexto que é atrelado a thread que o contexto for criado, pense num painel com botoes, switches, alavancas, etc... 
Isso seria um contexto, se você mudar alguma coisa, ele permanecerá pra sempre daquele jeito ate você mudar denovo, por exemplo se quiser usar uma textura:
```java
glBindTexture(GL_TEXTURE_2D, texture);
```
E sempre que for desenhar, ele irá usar essa textura, ate você mudar denovo, 
por isso você nao precisa usar o bind sempre que for desenhar (ao menos que tenha varios objetos que podem ter texturas diferentes)

## Não é uma Game Engine
O OpenGl não é uma game engine, tudo que você faz é preciso fazer manualmente,
por exemplo, o opengl não tem um sistema de coordenadas, você mesmo precisa de usar trigonometria pra fazer os seus objetos se moverem.

No opengl, não existe camera, a viewport fica sempre no mesmo lugar, mas os objetos se movem ao redor pra criar a ilusão de uma camera se movendo.

Você precisa criar uma mini engine pra fazer o seu projeto
## Pipeline
Todas as apis graficas incluindo o opengl, tem uma pipeline de renderização

Pense como uma função que você passa os vertices e ele retorna uma imagem

A pipeline é divida nos seguintes passos:

![image](https://user-images.githubusercontent.com/36360774/131265822-21bbd93a-d0a6-4804-838a-da3bc9f90dc9.png)

As etapas em azul são etapas programáveis, são chamados de Shaders e nós programamos usando o GLSL (GL Shading Language), é uma linguagem muito parecida com C.

Exemplo de codigo (Vertex shader):
```glsl
#version 330 core
layout (location = 0) in vec3 vertexPos;
void main(){
  gl_Position = vec4(vertexPos, 0.0);
}
```
Antigamente, no OpenGL 1.*, você não podia programar shaders, a pipeline era programada pelo proprio driver 
Isso limitava muito o que os programadores podiam fazer e tinha um overhead alto, apesar de ser mais facil de mexer
# Fim do capitulo 0
Proximo capitulo: [Criando uma janela](Chapter1-CreatingWindow.md)
