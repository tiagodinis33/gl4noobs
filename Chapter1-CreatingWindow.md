# Capitulo 1 - Criando uma janela

Agora que vamos começar a colocar a mão na massa!

Eu vou usar o GLFW para criar um contexto OpenGL usando Java, mas você tem uma variedade de opções de linguagens: [Veja aqui os bindings](README.md)

Primeiro precisamos inicializar o GLFW:
```java
import static org.lwjgl.glfw.GLFW.*;

public class Main {
    public static void main(String[] args){
        glfwInit();
    }
}
```
Depois precisamos de criar uma janela
```java
int largura = 800;
int altura = 600;
long idJanela = glfwCreateWindow(largura, altura, "gl4noobs - Capitulo 1", 0, 0)
```
Essa etapa pode falhar, por exemplo, se o usuario não tiver uma interface grafica rodando, ou talvez não tem o driver da placa de video funcionando.

Tem varios motivos, por isso é uma boa pratica checkar se a janela foi criada com sucesso.

```java
if(idJanela == 0){
    System.err.println("Não foi possivel criar uma janela!");
    System.exit(-1);
}
```
Agora é preciso criar um contexto OpenGL
Que é igualmente facil
```java
glfwMakeContextCurrent(idJanela);
```


Você agora deve pensar assim:
> Bom já que criamos uma janela e um contexto opengl, Agora deve rodar né?

E quando você roda, a janela abre e fecha...
O que está acontecendo?

Bom, a janela é criada mas não tem nenhum loop pra manter a janela aberta e gerenciar os eventos

Então vamos criar um loop:
```java
while(true){

}
```
E quando roda, a janela não está respondendo?

O gerenciador de janelas acha que o programa crashou porque não está respondendo aos eventos

Então vamos resolver isso adicionando um glfwPollEvents
```java
while(true){
    glfwPollEvents();
}
```
E pronto! 
Agora temos uma janela funcionando!

Mas ainda tem um problema, se você tentar fechar, você não consegue, porque o while é infinito,
ele nunca para, e para resolver isso vamos usar o glfwWindowShouldClose
```java
while(!glfwWindowShouldClose(idJanela)){
    glfwPollEvents();
}
```
Agora o while não é mais infinito! Ele irá parar se clicarmos no X.

# Wow! Você chegou ao fim do capitulo 1!
Proximo capitulo: [Capitulo 2 - Primeiro triangulo](Chapter2-FirstTriangle.md)
