# Capitulo 2 - Desenhando o primeiro triangulo!
Como eu to usando java, eu preciso de carregar o opengl antes de chamar as funções dele:
```java
GL.createCapabilities();
```
Pronto!
> **Nota:** é preciso chamar esse metodo depois de criar a janela e antes de usar alguma função opengl
> E essa parte depende do binding que você estiver utilizando!
## Coordenadas do opengl
No opengl, as coordenadas não sao definidas por pixeis,

Inves disso, ele pega a coordenada e multiplica pelo tamanho da tela
```
X em pixeis = x * (largura/2) + (largura/2)
Y em pixeis = y * (altura/2)+(altura/2)
```
Isso significa que as coordenadas em todas as dimensões vão de -1 até 1
Inclusive se a coordenada Z for maior que 1, ele será cortado!

Por isso, a imagem irá esticar se a janela não for um quadrado perfeito, porque o espaço depende da janela


## VBO (Vertex Buffer Object)

Um VBO é um pacote que tem um array de numeros

E pode ser criado com `glGenBuffers`

```java
int vboHandle = glGenBuffers();
```
> **Nota**: Todos os objetos do opengl são referenciados por um handle/id, que precisamos passar para as funções pra mexer no objeto

Agora podemos preencher o VBO com informação:
```java
glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
//Coordenadas para fazer um triangulo
float verticesPositions[] = {
    -0.5, -0.5,
    0.5, -0.5,
    0, 0.5
}
glBufferData(GL_ARRAY_BUFFER, verticesPositions, GL_STATIC_DRAW)
glBindBuffer(GL_ARRAY_BUFFER, 0)
```
Primeiro vinculamos o VBO no contexto OpenGL atual

Depois chamamos o `glBufferData` para preencher o buffer vinculado com informação.

O ultimo parametro é pra indicar para o opengl para que finalidade 
o buffer vai ser usado e qual a frequencia que vai ser atualizado.

Para atualizar, é so chamar o `glBufferData` denovo com nova informação

No final, é sempre bom desvincular o buffer, vinculando ao id 0

## VAO (Vertex Array Object)

O VAO é quase como um array de VBO's

É possivel vincular VBO's a ele com um index (0, 1, 2, etc...)

Para criar, é similar ao VBO, so que agora é com `glGenVertexArrays`
```java
int vaoHandle = glGenVertexArrays();
```
Agora precisamos vincular o vbo que criamos no exemplo acima com o VAO usando a função `glVertexAttribPointer`

```java
glBindVertexArray(vaoHandle);
glBindBuffer(GL_ARRAY_BUFFER, vboHandle);
glVertexAttribPointerArray(/*Posição do VBO no VAO*/ 0, /*Tamanho do vetor*/ 2, /*Tipo dos itens do array*/ GL_FLOAT, false, /*stride*/0,/*offset*/0)
glBindBuffer(GL_ARRAY_BUFFER, 0);
glBindVertexArray(0);
```
Agora podemos ter mais que um VBO, por exemplo, um VBO de cores para atribuir uma cor para cada vertice da malha

## A burrice da GPU
A GPU apesar de poder renderizar imagens com alta qualidade e performance, é uma maquina burra pra um cacete.

Ela só consegue desenhar triangulos!

Mesmo se você colocar pro opengl desenhar outras formas, o Opengl vai sempre transformar em triangulos para a GPU conseguir desenhar.
## Shaders

Bom, lembra dos shaders?

Agora pra desenhar esse triangulo, a gente precisa de um shader, no windows normalmente tem um shader padrao, mas no MacOS e linux, não tem.

Então você verá uma tela preta quando você tenta desenhar.

Vamos criar um shader então!
### Escrevendo o primeiro shader 
O OpenGL tem 2 shaders obrigatórios:
- Vertex shader: executado para cada vertice, usado para definir a posição de cada vertice, é nesta etapa que transformações são aplicadas (posição, rotação e escala)
- Fragment shader: executado para cada fragmento, usado para definir a cor do pixel

Primeiro colocamos a versão do glsl, no caso opengl 3.3
```glsl
#version 330 core
```
Agora, precisamos de colocar o input que contem a posição do vertice,
lembre-se que é rodado pra cada vertice!:
```glsl
#version 330 core
layout(location = 0) in vec2 position;
```
A configuração `location`, serve pra definir qual VBO do VAO a gente quer pegar, no caso o VBO que ta na posição 0 do VAO

Depois, precisamos de adicionar a função main para setar a variavel gl_Position
```glsl
#version 330 core
layout (location = 0) in vec2 position;
void main(){
    gl_Position = vec4(position, 0, 1);//vec4(position.x, position.y, 0, 1)
}
```
Como a position que vem do vertice é 2D e precisamos de setar o `gl_Position` que é 4d, temos que converter pra 4d com `vec4(position, /*z*/0,/*w*/ 1)`

O motivo de ser 4d e não 3d, é uma coisa que vamos falar nas transformações depois.

Agora precisamos de um fragment shader para retornar a cor dos pixeis

```glsl
#version 330 core
out vec4 FragColor;
void main(){
    FragColor = vec4(1,0,0,1);// 100% vermelho, 0% Verde, 0% Azul, 100% Opaco
}
```
> Nota: O FragColor está no formato `RGBA (Red Green Blue Alpha)`

Agora salve os dois shaders com o formato: `<nome>.glsl`, como estou criando em java, eu vou colocar dentro da pasta `src/main/resources` para que o arquivo fique dentro da jar.
E pronto! Você criou seus primeiros shaders!
### Compilando os shaders

Agora, precisamos de ler, compilar e linkar os dois arquivos que acabamos de criar em um `Program`

Se você programa em C/C++, Isso pode parecer familiar, porque é mesmo!

Mas a diferença é que a gente faz isso em runtime (tempo de execução).

Primeiro vamos criar um vertex shader no OpenGL com a função `glCreateShader`
```java
int vertexShader = glCreateShader(GL_VERTEX_SHADER);
```
Agora precisamos de ler os dois arquivos, e para isso vou criar um metodo:
```java
private static String readResourceAsString(String resource){
    InputStream is = Main.class.getResourceAsStream(resource);
    StringBuilder result = new StringBuilder();
     try (
            InputStreamReader streamReader = new InputStreamReader(is, StandardCharsets.UTF_8);
            BufferedReader reader = new BufferedReader(streamReader)
        ) {

            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

    } catch (IOException e) {
        e.printStackTrace();
    }
    return result.toString();
}
```
Agora é preciso mandar a src para o opengl compilar:
```java
int vertexShader = glCreateShader(GL_VERTEX_SHADER);
String source = readResourceAsString("/vertex.glsl");
glShaderSource(vertexShader, source);
```
Compilar...
```java
int vertexShader = glCreateShader(GL_VERTEX_SHADER);
String source = readResourceAsString("/vertex.glsl");
glShaderSource(vertexShader, source);
glCompileShader(vertexShader)
```
E pronto!
Mas agora precisamos de fazer a mesma coisa para o fragment shader:
```java
int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
String fragSource = readResourceAsString("/fragment.glsl");
glShaderSource(fragmentShader, fragSource);
glCompileShader(fragmentShader)
```
Agora é preciso linkar os dois shaders e formar um program que podemos usar quando formos desenhar,

Primeiro criamos um programa:
```java
int program = glCreateProgram(); 
```
Depois precisamos de vincular os shaders ao nosso program
```java
int program = glCreateProgram(); 
glAttachShader(program, vertexShader);
glAttachShader(program, fragmentShader);
```
Linkar...
```java
int program = glCreateProgram(); 
glAttachShader(program, vertexShader);
glAttachShader(program, fragmentShader);
glLinkProgram(program)
```
E depois disso, é sempre bom apagar os shaders porque ele ja foram compilados e linkados, e estão desperdiçando espaço na RAM:
```java
glDetachShader(program, vertexShader);
glDetachShader(program, fragmentShader);
glDeleteShader(vertexShader);
glDeleteShader(fragmentShader);
```
Agora sim temos um program, pronto para desenhar o nosso triangulo!

## Desenhando o nosso triangulo

Agora precisamos de desenhar o nosso triangulo.

Dentro do loop, nos precisamos de limpar a tela, porque ela vai estar com o ultimo frame:
```java
while(!glfwWindowShouldClose(idJanela)){
    glClear(GL_COLOR_BUFFER_BIT);
    //o codigo de renderização será aqui 
    glfwPollEvents();
}
```
Vinculamos o VAO e ativamos os VBO's dentro dele:
```java
glBindVertexArray(vaoHandle);
glEnableVertexAttribArray(0) //O VBO das posições dos vertices
```
Vincular o shader:
```java
glUseProgram(program);
```
Agora precisamos desenhar:
```java
glDrawArrays(GL_TRIANGLES/*Modo de desenho*/, 0/*Primeiro vertice*/, 3/*Quantidade de vertices para desenhar*/)
```
E no final desvincular tudo e trocar o backbuffer pelo buffer da tela (porque o GLFW usa Double Buffering):
```java
glUseProgram(0);
glDisableVertexAttrib(0);
glBindVertexArray(0);
glfwSwapBuffers(idJanela);
```

E pronto! Você deve ver um triangulo na tela:
![image](output.png)
# Wow! Você chegou ao final do capitulo 2!
Próximo capitulo: [Capitulo 3 - Transformações](Chapter3-Transformations.md)