<!-- Logo 4noobs -->

<p align="center">
  <a href="https://github.com/he4rt/4noobs" target="_blank">
    <img src=".github/header_4noobs.svg">
  </a>
</p>

<!-- Title -->

<p align="center">
  <h2 align="center">gl4noobs</h2>

  <h1 align="center"><img src=".github/OpenGL.svg" alt="Logo do OpenGL" width="120"></h1>
  
  <p align="center">
    <br />
    <a href="#ROADMAP"><strong>Explore a documentação »</strong></a>
    <br />
    <br />
    <a href="https://github.com/tiagodinis33/gl4noobs/issues">Report Bug</a>
    ·
    <a href="https://github.com/tiagodinis33/gl4noobs/issues">Request Feature</a>
  </p>
</p>
    
 <!-- ABOUT THE PROJECT -->

## Sobre o Projeto

Esse repositorio tem o objetivo de introduzir opengl para quem nunca mexeu com computação grafica antes

Requesitos:
  - uma linguagem de programação de sua preferencia (que tenha um binding pra opengl se não for uma linguagem compilada)
  - Algebra linear (matrizes, vetores, etc...), recomendo estudar isso pelo Khan Academy: https://www.khanacademy.org/math/linear-algebra/matrix-transformations?lang=pt
<!-- ROADMAP OF PROJECT -->

## ROADMAP

- [Capitulo 0 - Aquecimento](Chapter0-OpenGLConcepts.md)
- [Capitulo 1 - Criando uma janela](Chapter1-CreatingWindow.md)
- [Capitulo 2 - Primeiro triangulo](Chapter2-FirstTriangle.md)
  - [Coordenadas do opengl](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md#coordenadas-do-opengl)
  - [VBO (Vertex Buffer Object)](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md#vbo-vertex-buffer-object)
  - [VAO (Vertex Array Object)](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md#vao-vertex-array-object)
  - [A burrice da GPU](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md#a-burrice-da-gpu)
  - [Shaders](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md)
      - [Escrevendo o primeiro shader](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md#escrevendo-o-primeiro-shader)
      - [Compilando os shaders](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md#compilando-os-shaders)
  - [Desenhando o nosso triangulo](https://github.com/tiagodinis33/gl4noobs/blob/main/Chapter2-FirstTriangle.md#desenhando-o-nosso-triangulo)
- [Capitulo 3 - Transformações](Chapter3-Transformations.md)
  - [Matrix Identidade](Chapter3-Transformations.md#matrix-identidade)
  - [Perspectiva / Projeção 3D](Chapter3-Transformations.md#perspectiva--projeção-3d)
  - [Translação](Chapter3-Transformations.md#translação)
  - [Rotação](Chapter3-Transformations.md#rotação)
    - [Rotação no eixo X](Chapter3-Transformations.md#rotação-no-eixo-x)
    - [Rotação no eixo Y](Chapter3-Transformations.md#rotação-no-eixo-y)
    - [Rotação no eixo Z](Chapter3-Transformations.md#rotação-no-eixo-z)
  - [Escala](Chapter3-Transformations.md#escala)
<!-- CONTRIBUTING -->

## Como Contribuir

Contribuições fazem com que a comunidade open source seja um lugar incrível para aprender, inspirar e criar. Todas contribuições
são **extremamente apreciadas**

1. Realize um Fork do projeto
2. Crie um branch com a nova feature (`git checkout -b feature/featureBraba`)
3. Realize o Commit (`git commit -m 'Adicionado conteudo brabo'`)
4. Realize o Push no Branch (`git push origin feature/featureBraba`)
5. Abra um Pull Request

## Autores

- **tiagodinis33** - _Programador Java_


## Bindings de opengl
  - Java: [LWJGL](https://lwjgl.org)
  - C#: [OpenTK](https://www.nuget.org/packages/OpenTK/)
  - Python: 
      - OpenGL:
          - [ModernGL](https://github.com/cprogrammer1994/ModernGL)
          - [PyOpenGL](http://pyopengl.sourceforge.net/)
          - [PyOpenGLng](https://github.com/FabriceSalvaire/PyOpenGLng)
      - GLFW: [pyglfw-cffi](https://github.com/FabriceSalvaire/pyglfw-cffi)
  - JavaScript:
      - GLFW binding: https://github.com/mikeseven/node-glfw
      - OpenGL binding: https://github.com/paddybyers/node-gl
---
<p align="center">
  <a href="https://github.com/he4rt/4noobs" target="_blank">
    <img src=".github/footer_4noobs.svg" width="380">
  </a>
</p>
