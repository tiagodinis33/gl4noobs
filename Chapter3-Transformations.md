# Capitulo 3 - Transformações (Teoria)
Agora que nós temos um triangulo, é preciso agora usar a função glViewport para setar o tamanho da viewport pro tamanho da janela quando a janela for redimensionada.

E para isso vamos usar o `glfwSetWindowSizeCallback` para escutar pelo evento de redimensionamento.

```java
glfwSetWindowSizeCallback(idJanela, new GLFWWindowSizeCallback() {
		public void invoke(long window, int w, int h) {
			glViewport(0,0, w, h);
		}
	})
```
## Matriz Identidade
A matriz identidade é uma matriz que ao ser multiplicada por outra matriz ou vetor, ele permanece intocado, é como `x * 1 = x`

![img](https://quicklatex.com/cache3/41/ql_be6e08601c6bf820c624ee1688b2a541_l3.png)

## Perspectiva / Projeção 3D
Como citado no capitulo anterior, O triangulo vai esticar devido às coordenadas do opengl serem relativas ao tamanho da tela: [Clique aqui](Chapter2-FirstTriangle.md#coordenadas-do-opengl)
Para resolver isso, vamos usar a projeção de perpectiva para simular a visão humana

A imagem a seguir demonstra essa projeção:

![perspectiva](perspective_projection.png)

Pode parecer que não tem nada a ver, mas essa projeção consegue resolver o problema de as figuras esticarem por causa de o aspect ratio não ser 1:1 (quadrado)

![img](https://quicklatex.com/cache3/e8/ql_aa688c5ba2865a5e61feaaba633823e8_l3.png)

```
aspectRatio = width/height  
fov = Fov em radianos (radianos = graus * (PI/180))  
near = o plano proximo (Onde os vertices vao ser projetados)  
far = o plano distante (A distancia maxima que é visivel no eixo Z)  
```
E com esta matriz (4x4), é possivel criar a ilusão que objetos distantes são mais pequenos do que objetos que estão perto, e resolver o problema de objetos esticarem quando a tela não é um quadrado
E aqui vem a resposta para porque o opengl usa coordenadas 4d em vez de 3D, porque só é possivel multiplicar matrizes com vetores que tem o mesmo tamanho
o 4º componente do vetor, o opengl usa para dividir todas as coordenadas por esse numero
```glsl
void main()
{
    gl_Position = vec4(1,0,0,2); // = vec3(x/w,y/w, z/w) = vec3(1/2, 0/2, 0/2) = vec3(0.5, 0, 0)
}
```
## Translação
Como devem imaginar, para mover objetos, tambem precisamos de multiplicar por uma matriz:  
x = coordenada x  
y = coordenada y  
z = coordenada z

![img](https://quicklatex.com/cache3/4f/ql_234650ae8d5cf11266e43f6997f3584f_l3.png)

## Rotação
Esta matriz é um pouco mais complicada.  
**_θ_ = angulo**
### Rotação no eixo X

![img](https://quicklatex.com/cache3/b6/ql_14694818959ea5394a1b86c99d4657b6_l3.png)

### Rotação no eixo Y

![img](https://quicklatex.com/cache3/15/ql_acb66b78728070cba3f6d31315c54815_l3.png)

### Rotação no eixo Z

![img](https://quicklatex.com/cache3/1a/ql_b7b643f2e9b0320a28958fee2157051a_l3.png)

## Escala
Este é o mais facil dos dois, é so substituir os 1's da matriz de identidade pelos respectivos valores

![img](https://quicklatex.com/cache3/68/ql_3e0d108471b3bad126980339904d4e68_l3.png)

# Fim deste capitulo
Transformações é uma coisa um pouco complicada, mata qualquer um que não sabe o minimo de matemática  
Este capitulo foi bem cheio, vai tomar um cafezinho antes de ir para o proximo! :D  
Proximo capitulo: [Capitulo 4 - Transformações (Prática)](Chapter4-TransformationsInPractice.md)